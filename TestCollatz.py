#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase


from Collatz import (
    collatz_read,
    collatz_eval,
    collatz_print,
    collatz_solve,
    cycle_length,
)

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval((1, 10))
        self.assertEqual(v, (1, 10, 20))

    def test_eval_2(self):
        v = collatz_eval((100, 200))
        self.assertEqual(v, (100, 200, 125))

    def test_eval_3(self):
        v = collatz_eval((201, 210))
        self.assertEqual(v, (201, 210, 89))

    def test_eval_4(self):
        v = collatz_eval((900, 1000))
        self.assertEqual(v, (900, 1000, 174))

    def test_eval_5(self):
        v = collatz_eval((1, 1))
        self.assertEqual(v, (1, 1, 1))

    def test_eval_6(self):
        v = collatz_eval((100, 100))
        self.assertEqual(v, (100, 100, 26))

    def test_eval_7(self):
        v = collatz_eval((5000, 5003))
        self.assertEqual(v, (5000, 5003, 179))

    def test_eval_8(self):
        v = collatz_eval((200, 700))
        self.assertEqual(v, (200, 700, 145))

    def test_eval_9(self):
        v = collatz_eval((300, 2000))
        self.assertEqual(v, (300, 2000, 182))

    def test_eval_10(self):
        v = collatz_eval((1100, 2500))
        self.assertEqual(v, (1100, 2500, 209))

    def test_eval_11(self):
        v = collatz_eval((1000, 6000))
        self.assertEqual(v, (1000, 6000, 262))

    def test_eval_12(self):
        v = collatz_eval((200, 100))
        self.assertEqual(v, (200, 100, 125))

    # helper function

    def test_helper_1(self):
        v = cycle_length(5)
        self.assertEqual(v, 6)

    def test_helper_2(self):
        v = cycle_length(1)
        self.assertEqual(v, 1)

    def test_helper_3(self):
        v = cycle_length(100)
        self.assertEqual(v, 26)

    # -----
    # print
    # -----

    def test_print(self):
        sout = StringIO()
        collatz_print(sout, (1, 10, 20))
        self.assertEqual(sout.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        sin = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        sout = StringIO()
        collatz_solve(sin, sout)
        self.assertEqual(
            sout.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
