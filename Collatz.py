#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, Tuple
from metacache import meta

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> Tuple[int, int]:
    """
    read two ints
    s a string
    return a tuple of two ints
    """
    a = s.split()
    return int(a[0]), int(a[1])


# ------------
# collatz_eval
# ------------

cache = {}


def cycle_length(num):
    global cache
    assert num > 0
    original_num = num
    count = 1
    while num > 1:
        assert num > 0
        if (num % 2) == 0:
            num = num // 2
        else:
            # optimization so that odd number directly divides by 2 instead of looping again
            num = num + (num >> 1) + 1
            count += 1
        count += 1
    assert count > 0
    # store in cache as lazy cache optimization so they can be accessed later
    cache[original_num] = count
    assert count > 0
    return count


# optimization so that some cycle lengths are already stored
def eager_cache():
    global cache
    for i in range(1, 1000):
        n = cycle_length(i)
        cache[i] = n


def get_max(i, j) -> int:
    assert i > 0
    assert j > 0
    global cache
    # if i is less than half of j, can optimiza by starting at half
    half = (j // 2) + 1
    if i < half:
        i = half
    max = 0
    while i <= j:
        assert i > 0
        # check if cycle length is already in global cache
        if i in cache.keys():
            length = cache[i]
        else:
            # call helper for calculating cycle length
            length = cycle_length(i)
        if length > max:
            max = length
        i += 1
    assert max > 0
    return max


def collatz_eval(t: Tuple[int, int]) -> Tuple[int, int, int]:
    global cache
    i, j = t
    assert i > 0
    assert j > 0
    original_i = i
    original_j = j
    if i > j:
        temp = i
        i = j
        j = temp
    max = 0
    # if i is less than half of j, can optimiza by starting at half
    half = (j // 2) + 1
    if i < half:
        i = half
    # calculate the range of 1000s to check meta cache for
    ceiling = i + (1000 - i % 1000)
    orig_ceiling = ceiling
    floor = j - (j % 1000)
    # only have to check meta cache if at least 1000 apart
    if (floor - ceiling) > 1000:
        # keep checking until you get through the whole range
        while ceiling <= j:
            assert ceiling > 0
            lower = ceiling // 1000
            length = meta[lower]
            if length > max:
                max = length
            ceiling += 1000
        # also need to check range below
        below = get_max(i, orig_ceiling)
        if below > max:
            max = below
        # check range above
        above = get_max(floor, j)
        if above > max:
            max = above
        return original_i, original_j, max
    # if we don't need meta cache, then calculate the max the normal way
    max = get_max(i, j)
    assert max > 0
    return original_i, original_j, max


# -------------
# collatz_print
# -------------


def collatz_print(sout: IO[str], t: Tuple[int, int, int]) -> None:
    """
    print three ints
    sout a writer
    t a tuple of three ints
    """
    i, j, v = t
    sout.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(sin: IO[str], sout: IO[str]) -> None:
    """
    sin  a reader
    sout a writer
    """
    eager_cache()
    for s in sin:
        collatz_print(sout, collatz_eval(collatz_read(s)))
